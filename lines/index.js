const fs = require('fs')
const stream = require('stream')

const app = () => {
  let line_no = 1

  const read_from = process.argv.length < 3
    ? process.stdin
    : fs.createReadStream(process.argv[2])

  read_from
    .pipe(lineSplitter())
    .pipe(new stream.Transform({
      transform(buf_line, enc, next) {
        const line = buf_line.toString('utf-8')

        const result = isEven(line_no)
          ? line.toUpperCase()
          : line.toLowerCase()

        line_no++

        return next(null, result)
      }
    }))
    .pipe(process.stdout)
}

const isEven = x => x % 2 === 0

const lineSplitter = () => new Splitter('\n')

class Splitter extends stream.Transform {
  constructor(delim, opts = {}) {
    super(opts)

    this.delim = delim

    this.delim_size = typeof delim.length === 'number'
      ? delim.length
      : 1

    this.pending_part = null 
  }

  _transform(chunk, enc, next) {
    let start = 0
    let delim_at = chunk.indexOf(this.delim)

    while (delim_at >= 0) {
      const end = delim_at + this.delim_size

      const part = (() => {
        const cur_part = chunk.slice(start, end)

        if (this.pending_part !== null) {
          const full_part = Buffer.concat([this.pending_part, cur_part])
          this.pending_part = null
          
          return full_part
        }

        return cur_part
      })()
      
      this.push(part)

      start = end
      delim_at = chunk.indexOf(this.delim, start)
    }

    if (start < chunk.length) {
      this.pending_part = chunk.slice(start)
    }

    return next(null)
  }

  _flush(next) {
    if (this.pending_part !== null) {
      this.push(this.pending_part)
    }

    return next(null)
  }
}

app()
