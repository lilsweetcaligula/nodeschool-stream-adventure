const http = require('http')

const app = () => {
  const req = http.request({
    host: '127.0.0.1',
    port: 8099,
    method: 'POST'
  }, res => {
    return res.pipe(process.stdout)
  })

  return process.stdin.pipe(req)
}

app()
