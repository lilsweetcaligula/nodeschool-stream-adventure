const stream = require('stream')

const app = () => {
  process.stdin.pipe(concatStream(str => {
    console.log(reverseString(str))
  }))
}

const reverseString = str => [...str].reverse().join('')

const concatStream = (done, opts = {}) => new ConcatStream(buffer => {
  const str = buffer.toString('utf-8')
  return done(str)
}, opts)

class ConcatStream extends stream.Writable {
  constructor(done, opts = {}) {
    super(opts)

    this.once('finish', () => done(this.buffer))
    this.buffer = Buffer.from('')
  }

  _write(chunk, enc, next) {
    this.buffer = Buffer.concat([this.buffer, chunk])

    return next(null)
  }
}

app()
