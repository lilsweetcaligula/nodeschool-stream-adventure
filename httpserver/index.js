const http = require('http')
const path = require('path')
const stream = require('stream')

const app = () => {
  if (process.argv.length < 3) {
    console.error(`Usage: ${path.basename(__filename)} port`)
    return process.exit(1)
  }

  const port = process.argv[2]

  const server = http.createServer((req, res) => {
    if (req.method === 'POST') {
      return req
        .pipe(uppercaser())
        .pipe(res)
    }

    res.writeHead(404)
    return res.end()
  })

  return server.listen(port)
}

const uppercaser = () => 
  new stream.Transform({
    transform(chunk, enc, next) {
      const uppercased = chunk.toString('utf-8').toUpperCase()
      return next(null, uppercased)
    }
  })


app()
