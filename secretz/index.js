const crypto = require('crypto')
const fs = require('fs')
const path = require('path')
const stream = require('stream')
const tar = require('tar')

const app = () => {
  if (process.argv.length < 4) {
    console.error(`Usage: ${path.basename(__filename)} cipher passphrase`)
    return process.exit(1)
  }

  const cipher = process.argv[2]
  const passphrase = process.argv[3]

  process.stdin
    .pipe(crypto.createDecipher(cipher, passphrase))
    .pipe(tar.extract())
    .on('entry', entry => {
      if (entry.type !== 'File') {
        return entry.resume()
      }

      entry
        .pipe(crypto.createHash('md5', { encoding: 'hex' }))
        .pipe(new stream.Transform({
          transform(buffer, enc, next) {
            const hash = buffer.toString('utf-8')
            const result = [hash, entry.path].join(' ') + '\n'

            return next(null, result)
          }
        }))
        .pipe(process.stdout)
    })

  return
}

process.on('uncaughtException', err => {
  console.error(err.message)
  return process.exit(1)
})

app()
