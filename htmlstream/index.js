const stream = require('stream')
const trumpet = require('trumpet')

const app = () => {
  const trumpet_instance = trumpet()

  process.stdin
    .pipe(trumpet_instance)
    .pipe(process.stdout)

  trumpet_instance.selectAll('.loud', elem => {
    elem.createReadStream()
      .pipe(uppercaser())
      .pipe(elem.createWriteStream())
  })
}

const uppercaser = () => new stream.Transform({
  transform(chunk, enc, next) {
    return next(null, chunk.toString('utf-8').toUpperCase())
  }
})

app()
