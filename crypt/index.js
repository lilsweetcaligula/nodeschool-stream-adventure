const crypto = require('crypto')
const path = require('path')
const config = require('./config.js')

const app = () => {
  if (process.argv.length < 3) {
    console.error(`Usage: ${path.basename(__filename)} passphrase`)
    return process.exit(1)
  }

  const passphrase = process.argv[2]
  const algo = config.getCipherAlgo()
  const decipher = crypto.createDecipher(algo, passphrase)

  process.stdin
    .pipe(decipher)
    .pipe(process.stdout)

  return
}

process.on('uncaughtException', err => {
  console.error(err.message)
  return process.exit(1)
})

app()
