const fs = require('fs')
const zlib = require('zlib')
const stream = require('stream')
const streamCombiner = require('stream-combiner2')
const split = require('split2')

module.exports = () => streamCombiner(
  split(),
  jsonParser(),
  bookGroupper(),
  jsonStringifier({ newline_separated: true }),
  zlib.createGzip()
)

const bookGroupper = () => {
  const makeGroup = items => {
    if (items.length === 0) {
      throw new Error('Cannot make a group of empty items')
    }

    const [genre_info, ...book_infos] = items

    const genre = genre_info.name
    const book_names = book_infos.map(book => book.name)

    return { name: genre, books: book_names }
  }

  let acc = []

  const xform = new stream.Transform({ objectMode: true })

  xform._transform = function (item, enc, next) {
    if (item.type === 'genre') {
      if (acc.length > 0) {
        const group = makeGroup(acc)
        acc = []

        this.push(group)
      }

      acc.push(item)

      return next()
    }

    if (item.type === 'book') {
      acc.push(item)

      return next()
    }

    return next(new Error(`Unsupported item type: ${item.type}`))
  }

  xform._flush = function (cb) {
    const group = makeGroup(acc)
    this.push(group)

    return cb()
  }

  return xform
}

const jsonParser = () => new stream.Transform({
  readableObjectMode: true,

  transform(json, enc, next) {
    let obj

    try {
      obj = JSON.parse(json)
    } catch (err) {
      return next(err)
    }

    return next(null, obj)
  }
})

const jsonStringifier = (opts = {}) => new stream.Transform({
  writableObjectMode: true,

  transform(obj, enc, next) {
    const json = JSON.stringify(obj)
    const result = opts.newline_separated ? json + '\n' : json

    return next(null, result)
  }
})

