const { spawn } = require('child_process')
const duplexer = require('duplexer2')

module.exports = (cmd, args) => {
  const proc = spawn(cmd, args)
  return duplexer(proc.stdin, proc.stdout)
}
