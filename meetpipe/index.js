const path = require('path')
const fs = require('fs')

const app = () => {
  if (process.argv.length < 3) {
    console.error(`Usage: ${path.basename(__filename)} filepath`)
    return process.exit(1)
  }

  const filepath = process.argv[2]

  fs.createReadStream(filepath)
    .on('error', err => {
      switch (err.code) {
        case 'ENOENT':
          console.error(`No such file: ${filepath}`)
          return

        case 'EISDIR':
          console.error(`${filepath} must be a file, not a directory`)
          return

        default:
          throw err
      }
    })
    .pipe(process.stdout)
}

app()
