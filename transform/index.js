const stream = require('stream')

const app = () => {
  process.stdin.pipe(uppercaser()).pipe(process.stdout)
}

const uppercaser = () => new stream.Transform({
  transform(chunk, enc, cb) {
    const result = chunk.toString('utf-8').toUpperCase()
    return cb(null, result)
  }
})

app()
