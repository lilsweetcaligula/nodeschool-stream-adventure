const stream = require('stream')
const duplexer = require('duplexer2')

const countryCounter = counter => {
  const counts = {}

  const writable = new stream.Writable({
    objectMode: true,

    write(location, enc, next) {
      const { country } = location
      counts[country] = (counts[country] || 0) + 1

      return next()
    }
  })

  writable.once('finish', () => {
    counter.setCounts(counts)
  })

  return duplexer({ objectMode: true }, writable, counter)
}

module.exports = countryCounter
